package com.example.demomvapp.calculo;

public class CalculoVariacion {
    public CalculoVariacion() {
    }

    public static float calculoPorcentajeUF(float valorUF1, float valorUF2) {
        var calculoVariacion = valorUF2 - valorUF1;
        var calculoPorcentaje = (calculoVariacion * 100) / valorUF1;
        return calculoPorcentaje;
    }

}
