package com.example.demomvapp.controlador;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class ExtractorUF {
    private Document documento;

    public ExtractorUF() {
        try {
            documento = Jsoup.connect("https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=").get();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public float obtenerValorUF(int dia, String mes) {
        String id;
        var prefijo = "gr_ctl";
        if(dia >= 10){
            id = prefijo + (dia + 1) + "_" + mes;
        }else{
            id = prefijo + "0" + (dia + 1) + "_" + mes;
        }
        var elemento = documento.getElementById(id);
        String valor = elemento.html();
        valor = valor.replace(".", "");
        valor = valor.replace(",", ".");
        return Float.parseFloat(valor);
    }

    public String obtenerMes(int valorMes) {
        switch (valorMes) {
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            case 12:
                return "Diciembre";
            default:
                throw new IllegalStateException("Valor inesperado: " + valorMes);
        }
    }
}